package main

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"sort"
	"strconv"
	"testing"
	"time"
)

// вызов тяжелого процесса
func BenchmarkHeavyProcess(b *testing.B) {
	for i := 0; i < b.N; i++ {
		heavyProcess()
	}
}

// вызов генерации числа
func BenchmarkRandInt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		rand.Int()
	}
}

// вызов math sqrt 1000 000
func BenchmarkMathSqrt(b *testing.B) {
	for i := 0; i < b.N; i++ {
		math.Sqrt(1e6)
	}
}

// вызов функции для конкатенации строк
func BenchmarkFmtSprintf(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fmt.Sprintf("%s%s", "a", "b")
	}
}

// вызов функции утверждения типа
func BenchmarkTypeAssertion(b *testing.B) {
	var value interface{} = "example"
	for i := 0; i < b.N; i++ {
		_ = value.(string)
	}
}

// вызов метода sign с алгоритром sha256
func BenchmarkSign(b *testing.B) {
	for i := 0; i < b.N; i++ {
		sign("qwer", "asdf")
	}
}

// вызов функций для создания timestamp в типе string
func BenchmarkGenerateTimestamp(b *testing.B) {
	for i := 0; i < b.N; i++ {
		strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	}
}

// --- JSON Marshal ---
// вызов функции json.Marshal
func benchmarkJsonMarshal(value interface{}, b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		json.Marshal(value)
	}
}

// вызов функции json.Marshal с параметрами типа boolean, integer, string, struct
func BenchmarkJsonMarshalBoolean(b *testing.B) { benchmarkJsonMarshal(true, b) }
func BenchmarkJsonMarshalInteger(b *testing.B) { benchmarkJsonMarshal(100, b) }
func BenchmarkJsonMarshalString(b *testing.B)  { benchmarkJsonMarshal("example", b) }
func BenchmarkJsonMarshalStruct(b *testing.B) {
	data := struct {
		Text   string
		Number int
	}{"Example", 123}
	benchmarkJsonMarshal(data, b)
}

// --- JSON Unmarshal ---
// вызов функции json.Unmarshal
func benchmarkJsonUnmarshal(data []byte, b *testing.B) {
	var v interface{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		json.Unmarshal(data, &v)
	}
}

// вызов функции json.Unmarshal с параметрами типа boolean, integer, string, map
func BenchmarkJsonUnmarshalBoolean(b *testing.B) { benchmarkJsonUnmarshal([]byte("true"), b) }
func BenchmarkJsonUnmarshalInteger(b *testing.B) { benchmarkJsonUnmarshal([]byte("100"), b) }
func BenchmarkJsonUnmarshalString(b *testing.B)  { benchmarkJsonUnmarshal([]byte("\"example\""), b) }
func BenchmarkJsonUnmarshalMap(b *testing.B) {
	benchmarkJsonUnmarshal([]byte(`{"Text":"Example","Number":123}`), b)
}

// --- SUM ---
// вызов функции для вычисления суммы
func benchmarkSum(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		sum(n)
	}
}

// вызов функции для вычисления суммы чисел от 1 до 1000, 10 000 или 100 000
func BenchmarkSum1K(b *testing.B)   { benchmarkSum(1000, b) }
func BenchmarkSum10K(b *testing.B)  { benchmarkSum(1e4, b) }
func BenchmarkSum100K(b *testing.B) { benchmarkSum(1e5, b) }

// --- Fibonacci ---
// вызов функции для вычисления числа фибоначи
func benchmarkFindFibonacciNumber(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		findFibonacciNumber(n)
	}
}

// вызов функции для вычисления числа фибоначи, которые по счету находятся на 1, 5 10, 20, 40 месте
func BenchmarkFindFibonacciNumber1(b *testing.B)  { benchmarkFindFibonacciNumber(1, b) }
func BenchmarkFindFibonacciNumber5(b *testing.B)  { benchmarkFindFibonacciNumber(5, b) }
func BenchmarkFindFibonacciNumber10(b *testing.B) { benchmarkFindFibonacciNumber(10, b) }
func BenchmarkFindFibonacciNumber20(b *testing.B) { benchmarkFindFibonacciNumber(20, b) }
func BenchmarkFindFibonacciNumber40(b *testing.B) { benchmarkFindFibonacciNumber(40, b) }

// --- FILL SLICE ---
// вызов функции заполнения слайса
func benchmarkFillSlice(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		fillSlice(n, ASC)
	}
}

// вызов функции заполнения слайса 1000, 10 000, 100 000 элементов
func BenchmarkFillSlice1K(b *testing.B)   { benchmarkFillSlice(1000, b) }
func BenchmarkFillSlice10K(b *testing.B)  { benchmarkFillSlice(1e4, b) }
func BenchmarkFillSlice100K(b *testing.B) { benchmarkFillSlice(1e5, b) }

// --- FILL MAP ---
// вызов функции заполнения мапы
func benchmarkFillMap(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		fillMap(n)
	}
}

// вызов функции заполнения мапы 1000, 10 000, 100 000 элементов
func BenchmarkFillMap1K(b *testing.B)   { benchmarkFillMap(1000, b) }
func BenchmarkFillMap10K(b *testing.B)  { benchmarkFillMap(1e4, b) }
func BenchmarkFillMap100K(b *testing.B) { benchmarkFillMap(1e5, b) }

// --- FILL SYNC MAP ---
// вызов функции заполнения sync мапы
func benchmarkFillSyncMap(n int, b *testing.B) {
	for i := 0; i < b.N; i++ {
		fillSyncMap(n)
	}
}

// вызов функции заполнения sync мапы 1000, 10 000, 100 000 элементов
func BenchmarkFillSyncMap1K(b *testing.B)   { benchmarkFillSyncMap(1000, b) }
func BenchmarkFillSyncMap10K(b *testing.B)  { benchmarkFillSyncMap(1e4, b) }
func BenchmarkFillSyncMap100K(b *testing.B) { benchmarkFillSyncMap(1e5, b) }

// --- TRAVERSE SLICE ---
// проход по слайсу
func benchmarkTraverseSlice(n int, b *testing.B) {
	arr := fillSlice(n, ASC)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		traverseSlice(arr)
	}
}

// проход по слайсу 1000, 10 000, 100 000 элементов
func BenchmarkTraverseSlice1K(b *testing.B)   { benchmarkTraverseSlice(1000, b) }
func BenchmarkTraverseSlice10K(b *testing.B)  { benchmarkTraverseSlice(1e4, b) }
func BenchmarkTraverseSlice100K(b *testing.B) { benchmarkTraverseSlice(1e5, b) }

// --- TRAVERSE MAP ---
// проход по мапе
func benchmarkTraverseMap(n int, b *testing.B) {
	data := fillMap(n)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		traverseMap(data)
	}
}

// проход по мапе 1000, 10 000, 100 000 элементов
func BenchmarkTraverseMap1K(b *testing.B)   { benchmarkTraverseMap(1000, b) }
func BenchmarkTraverseMap10K(b *testing.B)  { benchmarkTraverseMap(1e4, b) }
func BenchmarkTraverseMap100K(b *testing.B) { benchmarkTraverseMap(1e5, b) }

// --- TRAVERSE SYNC MAP ---
// проход по sync мапе
func benchmarkTraverseSyncMap(n int, b *testing.B) {
	data := fillSyncMap(n)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		traverseSyncMap(data)
	}
}

// проход по sync мапе 1000, 10 000, 100 000 элементов
func BenchmarkTraverseSyncMap1K(b *testing.B)   { benchmarkTraverseSyncMap(1000, b) }
func BenchmarkTraverseSyncMap10K(b *testing.B)  { benchmarkTraverseSyncMap(1e4, b) }
func BenchmarkTraverseSyncMap100K(b *testing.B) { benchmarkTraverseSyncMap(1e5, b) }

// --- SORT ---
// сортировка массива
func benchmarkSort(sortFunction func([]int), n int, direction string, b *testing.B) {
	arr := fillSlice(n, direction)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		sortFunction(arr)
	}
}

// --- BUBBLE SORT ---
// пузырьковая сортировка с массивом из 1000, 10 000, 100 000 элементов, заполненных в порядке уменьшения(DESC)
func BenchmarkBubbleSortDesc1K(b *testing.B)   { benchmarkSort(bubbleSort, 1000, DESC, b) }
func BenchmarkBubbleSortDesc10K(b *testing.B)  { benchmarkSort(bubbleSort, 1e4, DESC, b) }
func BenchmarkBubbleSortDesc100K(b *testing.B) { benchmarkSort(bubbleSort, 1e5, DESC, b) }

// пузырьковая сортировка с массивом из 1000, 10 000, 100 000 элементов, заполненных в порядке увеличения(ASC)
func BenchmarkBubbleSortAsc1K(b *testing.B)   { benchmarkSort(bubbleSort, 1000, ASC, b) }
func BenchmarkBubbleSortAsc10K(b *testing.B)  { benchmarkSort(bubbleSort, 1e4, ASC, b) }
func BenchmarkBubbleSortAsc100K(b *testing.B) { benchmarkSort(bubbleSort, 1e5, ASC, b) }

// --- BUILT-IN SORT (под капотом использует quickSort) ---
// встроенная в язык сортировка с массивом из 1000, 10 000, 100 000 элементов, заполненных в порядке уменьшения(DESC)
func BenchmarkBuiltInSortDesc1K(b *testing.B)   { benchmarkSort(sort.Ints, 1000, DESC, b) }
func BenchmarkBuiltInSortDesc10K(b *testing.B)  { benchmarkSort(sort.Ints, 1e4, DESC, b) }
func BenchmarkBuiltInSortDesc100K(b *testing.B) { benchmarkSort(sort.Ints, 1e5, DESC, b) }

// встроенная в язык сортировка с массивом из 1000, 10 000, 100 000 элементов, заполненных в порядке увеличения(ASC)
func BenchmarkBuiltInSortAsc1K(b *testing.B)   { benchmarkSort(sort.Ints, 1000, ASC, b) }
func BenchmarkBuiltInSortAsc10K(b *testing.B)  { benchmarkSort(sort.Ints, 1e4, ASC, b) }
func BenchmarkBuiltInSortAsc100K(b *testing.B) { benchmarkSort(sort.Ints, 1e5, ASC, b) }
