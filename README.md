# Тест производительности

Цель: Прооект был создан для теста производительности процессоров с архитектурами **AMD** и **ARM**

>- main.go - хранятся методы, без main() функции
>- benchmark_test.go - все бенчмарк методы
>- bench.txt, cpu.out генерируются при запуске команды:
`go test -bench=. -benchtime=5s -count 5 -benchmem -timeout=2h -cpuprofile=cpu.out . | tee bench.txt
`
>- test.test также генерируется, но это не нужный файл
>- после получения cpu.out можно запустить graphviz: `go tool pprof -http :8080 cpu.out`

2 версия: https://gitlab.com/a.zakrikchinov/performance-test/-/tree/v2