package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"runtime"
	"sync"
	"time"
)

const (
	ASC  = "ASC"  // в сторону увеличения
	DESC = "DESC" // в сторону уменьшения
)

// вызов данной функции нагружает ядра процессора на 100%
func heavyProcess() {
	done := make(chan int)
	for i := 0; i < runtime.NumCPU(); i++ {
		go func() {
			for {
				select {
				case <-done:
					return
				default:
				}
			}
		}()
	}
	time.Sleep(time.Second * 5)
	close(done)
}

// вызов метода sign с алгоритром sha256
func sign(data, secret string) string {
	key := []byte(secret)
	h := hmac.New(sha256.New, key)
	h.Write([]byte(data))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

// считает сумму от 1 до n
func sum(n int) int {
	res := 0
	for i := 1; i <= n; i++ {
		res += i
	}
	return res
}

// вызов рекурсивной функции для вычисления числа фибоначи
func findFibonacciNumber(n int) int {
	if n < 2 {
		return n
	}
	return findFibonacciNumber(n-1) + findFibonacciNumber(n-2)
}

// --- FILL SLICE, MAP, SYNC MAP ---
// массив заполняется ASC или DESC порядке
// не используются рандомные значения, потому что сравниваются 2 архитектуры и исходные данные должны быть похожи
func fillSlice(num int, direction string) (arr []int) {
	if direction == DESC {
		for i := num; i > 0; i-- { // заполняет массив от num(включительно) до 1
			arr = append(arr, i)
		}
	} else { // direction == ASC
		for i := 1; i <= num; i++ { // заполняет массив от 1 до num(включительно)
			arr = append(arr, i)
		}
	}
	return arr
}

// заполняет ключи мапы значениями от 1 до num (включительно)
func fillMap(num int) map[int]bool {
	data := make(map[int]bool)
	for i := 1; i <= num; i++ { // заполняет массив от 1 до num(включительно)
		data[i] = true
	}
	return data
}

// заполняет ключи sync мапы значениями от 1 до num (включительно)
func fillSyncMap(num int) *sync.Map {
	data := new(sync.Map)
	for i := 1; i <= num; i++ { // заполняет массив от 1 до num(включительно)
		data.Store(i, true)
	}
	return data
}

// --- TRAVERSE SLICE, MAP, SYNC MAP ---
// проход по слайсу
func traverseSlice(arr []int) {
	for range arr {
	}
}

// проход по мапе
func traverseMap(data map[int]bool) {
	for range data {
	}
}

// проход по sync мапе
func traverseSyncMap(data *sync.Map) {
	data.Range(func(k, v interface{}) bool {
		return true // if false, Range stops
	})
}

// --- SORT ---
// пузырьковая сортировка в сторону увеличения
func bubbleSort(arr []int) {
	length := len(arr)
	for i := 0; i < length-1; i++ {
		for j := 0; j < length-i-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}
}
